#!/usr/bin/python

from datetime import datetime
current_date=datetime.now().strftime("%Y-%m-%d")
import pandas
import subprocess
import glob
import os
path = "/home/demipt2/drnc"



print ("Beginning the ETL process...")

#2  Connection to SQL

import jaydebeapi
conn = jaydebeapi.connect('oracle.jdbc.driver.OracleDriver','jdbc:oracle:thin:demipt2/peregrintook@de-oracle.chronosavant.ru:1521/deoracle',['demipt2','peregrintook'],'/home/demipt2/ojdbc8.jar')
conn.jconn.setAutoCommit(False)
curs = conn.cursor()

#3 Staging clearance

curs.execute("delete from demipt2.DRNC_STG_ACCOUNTS")
curs.execute("delete from demipt2.DRNC_STG_ACCOUNTS_DEL")
curs.execute("delete from demipt2.DRNC_STG_CARDS")
curs.execute("delete from demipt2.DRNC_STG_CARDS_DEL")
curs.execute("delete from demipt2.DRNC_STG_CLIENTS")
curs.execute("delete from demipt2.DRNC_STG_CLIENTS_DEL")
curs.execute("delete from demipt2.DRNC_STG_TERMINALS")
curs.execute("delete from demipt2.DRNC_STG_TERMINALS_DEL")


#3 clearance of Table Terminals Source

curs.execute("delete from demipt2.DRNC_SOURCE_TERMINALS")

#3 Capture new data from source to staging

#Cards
curs.execute("""insert into demipt2.DRNC_STG_CARDS (CARD_NUM, ACCOUNT, UPDATE_DT)
select replace(CARD_NUM,' ',''), replace(ACCOUNT,' ',''),coalesce(UPDATE_DT,CREATE_DT) from bank.cards
where coalesce (update_dt,create_dt) > ( 
    select coalesce( last_update_dt, to_date( '1900-01-01', 'YYYY-MM-DD') ) 
    from demipt2.drnc_meta_bank where table_db = 'DEMIPT2' and table_name = 'CARDS' )
 """)
#Accounts
curs.execute("""
    insert into demipt2.DRNC_STG_ACCOUNTS (ACCOUNT, VALID_TO, CLIENT, UPDATE_DT)
select replace(ACCOUNT,' ',''), VALID_TO, CLIENT, coalesce(UPDATE_DT,CREATE_DT)  from bank.accounts
where coalesce (update_dt,create_dt) > ( 
    select coalesce( last_update_dt, to_date( '1900-01-01', 'YYYY-MM-DD') ) 
    from demipt2.drnc_meta_bank where table_db = 'DEMIPT2' and table_name = 'ACCOUNTS' )
""")
 
#Clients
curs.execute("""insert into demipt2.DRNC_STG_CLIENTS (CLIENT_ID, LAST_NAME, FIRST_NAME, PATRONYMIC, DATE_OF_BIRTH, PASSPORT_NUM, PASSPORT_VALID_TO, PHONE, UPDATE_DT)
select CLIENT_ID, LAST_NAME, FIRST_NAME, PATRONYMIC, DATE_OF_BIRTH, replace(PASSPORT_NUM,' ',''), PASSPORT_VALID_TO, PHONE, coalesce(UPDATE_DT,CREATE_DT)  from bank.clients
where coalesce (update_dt,create_dt) > ( 
    select coalesce( last_update_dt, to_date( '1900-01-01', 'YYYY-MM-DD') ) 
    from demipt2.drnc_meta_bank where table_db = 'DEMIPT2' and table_name = 'CLIENTS' )
""")
#Terminals

# looking for excel file in a folder

terminal_files=glob.glob(os.path.join(path , "terminals*.xlsx"))

#upload list of terminals from excel to dataframe TL
for f in terminal_files:
    tl=pandas.read_excel(f,sheet_name='terminals', header=0)
# for now we will only upload the whole list without exctracting increment - should investigate options in python
# upload data from dataframe TL to SqL source table
curs.executemany( "insert into demipt2.drnc_source_terminals (terminal_id, terminal_type,terminal_city,terminal_address,update_dt) values (?,?,?,?,current_date)",tl.values.tolist())

curs.execute("""insert into demipt2.DRNC_STG_TERMINALS (terminal_id,terminal_type,terminal_city,terminal_address,update_dt)
select t.terminal_id,t.terminal_type,t.terminal_city,t.terminal_address,t.update_dt from demipt2.DRNC_SOURCE_TERMINALS t
left join demipt2.DRNC_DWH_DIM_TERMINALS_HIST s
on s.terminal_id = t.terminal_id and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
where  t.terminal_id is null or t.terminal_id is not null and ( 1=0
            or t.TERMINAL_TYPE <> s.TERMINAL_TYPE or ( s.TERMINAL_TYPE is null and t.TERMINAL_TYPE is not null ) or ( s.TERMINAL_TYPE is not null and t.TERMINAL_TYPE is null )
            or t.TERMINAL_CITY <> s.TERMINAL_CITY or ( s.TERMINAL_CITY is null and t.TERMINAL_CITY is not null ) or ( s.TERMINAL_CITY is not null and t.TERMINAL_CITY is null )
            or t.TERMINAL_ADDRESS <> s.TERMINAL_ADDRESS or ( s.TERMINAL_ADDRESS is null and t.TERMINAL_ADDRESS is not null ) or ( s.TERMINAL_ADDRESS is not null and t.TERMINAL_ADDRESS is null )
        )
        and update_dt > ( 
    select coalesce( last_update_dt, to_date( '1900-01-01', 'YYYY-MM-DD') ) 
    from demipt2.drnc_meta_bank where table_db = 'DEMIPT2' and table_name = 'TERMINALS' )
""")

#Transactions
#looking for csv file in a folder
trans_files=glob.glob(os.path.join(path , "transactions*.txt"))

#uploading daily transactions from excel to DF PST 
for f in trans_files:
    trn = pandas.read_csv(f, decimal=",", sep = ";", header=0, dtype={"transaction_id": str, "transaction_date": str, "amount":float,"card_num":str,"oper_type":str,"oper_result":str,"terminal":str})
trn2=trn[['transaction_id','transaction_date','card_num','oper_type','amount','oper_result','terminal']]
trn2['card_num'] = trn2['card_num'].replace(r'\s+','',regex=True)

#upload data from dataframe trn2 to SqL staging table
curs.executemany( "insert into DEMIPT2.DRNC_DWH_FACT_TRANSACTIONS (trans_id,trans_date,card_num,oper_type,amt,oper_result,terminal_id) values (?,to_date(?,'YYYY-MM-DD HH24:MI:SS'),?,?,?,?,?)",trn2.values.tolist())


#Passports
# looking for excel file in a folder
pass_files=glob.glob(os.path.join(path , "passport_blacklist*.xlsx"))
#uploading passport data from excel to DF PST 
for f in pass_files:
    pst=pandas.read_excel(f,sheet_name='blacklist', header=0, dtype={"passport": str, "date": str})
pst2=pst[['passport','date']] #changing the order of columns (probably should look for more optimal way)

pst2['passport'] = pst2['passport'].replace(r'\s+','',regex=True)

#upload data from dataframe PST to SqL staging table

curs.executemany( "insert into demipt2.drnc_stg_pssprt_blcklst (passport_num, entry_dt) values (?,to_date(?,'YYYY-MM-DD HH24:MI:SS'))",pst2.values.tolist())


#4 Insert data to capture deletions
#for sql tables
curs.execute ("insert into demipt2.DRNC_STG_CARDS_DEL ( CARD_NUM )select replace(CARD_NUM,' ','') from bank.cards")
curs.execute ("insert into demipt2.DRNC_STG_ACCOUNTS_DEL ( ACCOUNT )select replace(ACCOUNT,' ','') from bank.accounts")
curs.execute ("insert into demipt2.DRNC_STG_CLIENTS_DEL ( CLIENT_ID )select CLIENT_ID from bank.clients")
curs.execute ("insert into demipt2.DRNC_STG_TERMINALS_DEL ( terminal_id )select terminal_id from demipt2.DRNC_SOURCE_TERMINALS")



#6 Merge and insert into DWH Tables of new or changed data
# Table Cards  old version update
curs.execute("""merge into demipt2.DRNC_DWH_DIM_CARDS_HIST tgt
using (
    select 
        s.CARD_NUM,
        s.ACCOUNT,
        s.UPDATE_DT
    from demipt2.drnc_stg_cards s
    left join demipt2.DRNC_DWH_DIM_CARDS_HIST t
    on s.CARD_NUM = t.CARD_NUM 
    where 
        t.CARD_NUM is not null and ( 1=0
            or t.ACCOUNT <> s.ACCOUNT or ( s.ACCOUNT is null and t.ACCOUNT is not null ) or ( s.ACCOUNT is not null and t.ACCOUNT is null )
        and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
        )
) stg
on ( tgt.CARD_NUM = stg.CARD_NUM)
when matched then update set effective_to = stg.update_dt - interval '1' second where effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
""")
# Cards - new version insert
curs.execute ("""insert into demipt2.DRNC_DWH_DIM_CARDS_HIST ( CARD_NUM, ACCOUNT, EFFECTIVE_FROM, EFFECTIVE_TO, DELETED_FLG )
select 
    CARD_NUM, 
    ACCOUNT, 
    update_dt as effective_from, 
    to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
    'N' as deleted_flg
from demipt2.DRNC_STG_CARDS
""")

# Table Accounts  old version update
curs.execute("""merge into demipt2.DRNC_DWH_DIM_ACCOUNTS_HIST tgt
using (
    select 
            s.ACCOUNT,
            s.VALID_TO,
            s.CLIENT,
            s.UPDATE_DT
    from demipt2.drnc_stg_accounts s
    left join demipt2.DRNC_DWH_DIM_ACCOUNTS_HIST t
    on s.ACCOUNT = t.ACCOUNT 
    where 
        t.ACCOUNT is not null and ( 1=0
            or t.VALID_TO <> s.VALID_TO or ( s.VALID_TO is null and t.VALID_TO is not null ) or ( s.VALID_TO is not null and t.VALID_TO is null )
            or t.CLIENT <> s.CLIENT or ( s.CLIENT is null and t.CLIENT is not null ) or ( s.CLIENT is not null and t.CLIENT is null )
       and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
        )
) stg
on ( tgt.ACCOUNT = stg.ACCOUNT)
when matched then update set effective_to = stg.update_dt - interval '1' second where effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
""")
# Table Accounts new version insert
curs.execute("""insert into demipt2.DRNC_DWH_DIM_ACCOUNTS_HIST ( ACCOUNT, VALID_TO, CLIENT, EFFECTIVE_FROM, EFFECTIVE_TO, DELETED_FLG )
select 
    ACCOUNT, 
    VALID_TO,
    CLIENT, 
    update_dt as effective_from, 
    to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
    'N' as deleted_flg
from demipt2.drnc_stg_accounts
""")
# Table Clients old version update
curs.execute("""merge into demipt2.DRNC_DWH_DIM_CLIENTS_HIST tgt
using (
    select 
            s.CLIENT_ID,
            s.LAST_NAME,
            s.FIRST_NAME,
            s.PATRONYMIC,
            s.DATE_OF_BIRTH,
            s.PASSPORT_NUM,
            s.PASSPORT_VALID_TO,
            s.PHONE,
            s.UPDATE_DT
    from demipt2.drnc_stg_clients s
    left join demipt2.DRNC_DWH_DIM_CLIENTS_HIST t
    on s.CLIENT_ID = t.CLIENT_ID 
    where 
        t.CLIENT_ID is not null and ( 1=0
            or t.LAST_NAME <> s.LAST_NAME or ( s.LAST_NAME is null and t.LAST_NAME is not null ) or ( s.LAST_NAME is not null and t.LAST_NAME is null )
            or t.FIRST_NAME <> s.FIRST_NAME or ( s.FIRST_NAME is null and t.FIRST_NAME is not null ) or ( s.FIRST_NAME is not null and t.FIRST_NAME is null )
            or t.PATRONYMIC <> s.PATRONYMIC or ( s.PATRONYMIC is null and t.PATRONYMIC is not null ) or ( s.PATRONYMIC is not null and t.PATRONYMIC is null )
            or t.DATE_OF_BIRTH <> s.DATE_OF_BIRTH or ( s.DATE_OF_BIRTH is null and t.DATE_OF_BIRTH is not null ) or ( s.DATE_OF_BIRTH is not null and t.DATE_OF_BIRTH is null )
            or t.PASSPORT_NUM <> s.PASSPORT_NUM or ( s.PASSPORT_NUM is null and t.PASSPORT_NUM is not null ) or ( s.PASSPORT_NUM is not null and t.PASSPORT_NUM is null )
            or t.PASSPORT_VALID_TO <> s.PASSPORT_VALID_TO or ( s.PASSPORT_VALID_TO is null and t.PASSPORT_VALID_TO is not null ) or ( s.PASSPORT_VALID_TO is not null and t.PASSPORT_VALID_TO is null )
            or t.PHONE <> s.PHONE or ( s.PHONE is null and t.PHONE is not null ) or ( s.PHONE is not null and t.PHONE is null )
        )
        and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
) stg
on ( tgt.CLIENT_ID = stg.CLIENT_ID)
when matched then update set effective_to = stg.update_dt - interval '1' second where effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
""")
# Table clients new version insert
curs.execute("""insert into demipt2.DRNC_DWH_DIM_CLIENTS_HIST
  ( CLIENT_ID, LAST_NAME, FIRST_NAME, PATRONYMIC, DATE_OF_BIRTH, PASSPORT_NUM, PASSPORT_VALID_TO, PHONE, EFFECTIVE_FROM, EFFECTIVE_TO, DELETED_FLG )
select 
    CLIENT_ID,
    LAST_NAME,
    FIRST_NAME,
    PATRONYMIC,
    DATE_OF_BIRTH,
    PASSPORT_NUM,
    PASSPORT_VALID_TO,
    PHONE,
    update_dt as effective_from, 
    to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
    'N' as deleted_flg
from demipt2.drnc_stg_clients
""")
# Table Terminals old version update 
curs.execute("""merge into demipt2.DRNC_DWH_DIM_TERMINALS_HIST tgt
using (
    select 
            s.terminal_id,
            s.terminal_type,
            s.terminal_city,
            s.terminal_address,
            s.UPDATE_DT
    from demipt2.drnc_stg_TERMINALS s
    left join demipt2.DRNC_DWH_DIM_TERMINALS_HIST t
    on s.terminal_id = t.terminal_id 
    where 
        t.terminal_id is not null and ( 1=0
            or t.TERMINAL_TYPE <> s.TERMINAL_TYPE or ( s.TERMINAL_TYPE is null and t.TERMINAL_TYPE is not null ) or ( s.TERMINAL_TYPE is not null and t.TERMINAL_TYPE is null )
            or t.TERMINAL_CITY <> s.TERMINAL_CITY or ( s.TERMINAL_CITY is null and t.TERMINAL_CITY is not null ) or ( s.TERMINAL_CITY is not null and t.TERMINAL_CITY is null )
            or t.TERMINAL_ADDRESS <> s.TERMINAL_ADDRESS or ( s.TERMINAL_ADDRESS is null and t.TERMINAL_ADDRESS is not null ) or ( s.TERMINAL_ADDRESS is not null and t.TERMINAL_ADDRESS is null )
        )
        and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
) stg
on ( tgt.terminal_id = stg.terminal_id)
when matched then update set effective_to = stg.update_dt - interval '1' second where effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
""")
# Table Terminals new version insert
curs.execute("""insert into demipt2.DRNC_DWH_DIM_TERMINALS_HIST
  ( terminal_id,terminal_type,terminal_city,terminal_address, EFFECTIVE_FROM, EFFECTIVE_TO, DELETED_FLG )
select 
    terminal_id,
    terminal_type,
    terminal_city,
    terminal_address,
    update_dt as effective_from, 
    to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
    'N' as deleted_flg
from demipt2.drnc_stg_TERMINALS
""")

#11 Deletions
# Insert deleted data to table Cards
curs.execute ("""insert into demipt2.DRNC_DWH_DIM_CARDS_HIST ( CARD_NUM, ACCOUNT, effective_from, effective_to, deleted_flg )
select 
   CARD_NUM, 
    ACCOUNT, 
    current_date as effective_from, 
    to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
    'Y' as deleted_flg
from (
    select
        t.CARD_NUM,
        t.ACCOUNT
    from demipt2.DRNC_DWH_DIM_CARDS_HIST t
    left join demipt2.DRNC_STG_CARDS_DEL s
    on t.CARD_NUM = s.CARD_NUM 
    where s.CARD_NUM  is null  and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
)
""")
# Update all version of deleted data in Table Cards
curs.execute("""update demipt2.DRNC_DWH_DIM_CARDS_HIST
set effective_to = current_date - interval '1' second
where CARD_NUM in (
    select
        t.CARD_NUM
    from demipt2.DRNC_DWH_DIM_CARDS_HIST t
    left join demipt2.DRNC_STG_CARDS_DEL s
    on t.CARD_NUM = s.CARD_NUM 
    where s.CARD_NUM is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
)
and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
and deleted_flg = 'N'
""")
# Insert deleted data to table Accounts
curs.execute("""insert into demipt2.DRNC_DWH_DIM_ACCOUNTS_HIST ( ACCOUNT, VALID_TO, CLIENT, EFFECTIVE_FROM, EFFECTIVE_TO, DELETED_FLG)
select 
    ACCOUNT, 
    VALID_TO, 
    CLIENT,
    current_date as effective_from, 
    to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
    'Y' as deleted_flg
from (
    select
        t.ACCOUNT,
        t.VALID_TO,
        t.CLIENT
    from demipt2.DRNC_DWH_DIM_ACCOUNTS_HIST t
    left join demipt2.drnc_stg_accounts_del s
    on t.ACCOUNT = s.ACCOUNT  
    where s.ACCOUNT  is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
)
""")
# Update old version of deleted data in Table Accounts
curs.execute("""update demipt2.DRNC_DWH_DIM_ACCOUNTS_HIST
set effective_to = current_date - interval '1' second
where ACCOUNT in (
    select
        t.ACCOUNT
    from demipt2.DRNC_DWH_DIM_ACCOUNTS_HIST t
    left join demipt2.drnc_stg_accounts_del s
    on t.ACCOUNT= s.ACCOUNT 
    where s.ACCOUNT is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
)
and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
and deleted_flg = 'N'
""")

# Insert deleted data to table Clients
curs.execute("""insert into demipt2.DRNC_DWH_DIM_CLIENTS_HIST
( CLIENT_ID, LAST_NAME, FIRST_NAME, PATRONYMIC, DATE_OF_BIRTH, PASSPORT_NUM, PASSPORT_VALID_TO, PHONE, EFFECTIVE_FROM, EFFECTIVE_TO, DELETED_FLG )
select 
    CLIENT_ID,
    LAST_NAME,
    FIRST_NAME,
    PATRONYMIC,
    DATE_OF_BIRTH,
    PASSPORT_NUM,
    PASSPORT_VALID_TO,
    PHONE,
    current_date as effective_from, 
    to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
    'Y' as deleted_flg
from (
    select
            t.CLIENT_ID,
            t.LAST_NAME,
            t.FIRST_NAME,
            t.PATRONYMIC,
            t.DATE_OF_BIRTH,
            t.PASSPORT_NUM,
            t.PASSPORT_VALID_TO,
            t.PHONE
    from demipt2.DRNC_DWH_DIM_CLIENTS_HIST t
    left join demipt2.drnc_stg_clients_del s
    on t.CLIENT_ID = s.CLIENT_ID 
    where s.CLIENT_ID  is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
)
""")

# Update old version of deleted data in Table Clients
curs.execute("""update demipt2.DRNC_DWH_DIM_CLIENTS_HIST
set effective_to = current_date - interval '1' second
where CLIENT_ID in (
    select
        t.CLIENT_ID
    from demipt2.DRNC_DWH_DIM_CLIENTS_HIST t
    left join demipt2.drnc_stg_clients_del s
    on t.CLIENT_ID= s.CLIENT_ID 
    where s.CLIENT_ID is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
)
and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
and deleted_flg = 'N'
""")

# Insert deleted data to table Terminals

curs.execute("""insert into demipt2.DRNC_DWH_DIM_TERMINALS_HIST
( terminal_id,terminal_type,terminal_city,terminal_address, EFFECTIVE_FROM, EFFECTIVE_TO, DELETED_FLG )
select 
    terminal_id,
    terminal_type,
    terminal_city,
    terminal_address,
    current_date as effective_from, 
    to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
    'Y' as deleted_flg
from (
    select
            t.terminal_id,
            t.terminal_type,
            t.terminal_city,
            t.terminal_address
    from demipt2.DRNC_DWH_DIM_TERMINALS_HIST t
    left join demipt2.drnc_stg_terminals_del s
    on t.terminal_id = s.terminal_id 
    where s.terminal_id  is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
)
""")
# Update old version of deleted data in Table Terminals
curs.execute("""update demipt2.DRNC_DWH_DIM_TERMINALS_HIST
set effective_to = current_date - interval '1' second
where terminal_id in (
    select
        t.terminal_id
    from demipt2.DRNC_DWH_DIM_TERMINALS_HIST t
    left join demipt2.drnc_stg_terminals_del s
    on t.terminal_id= s.terminal_id 
    where s.terminal_id is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
)
and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
and deleted_flg = 'N'
""")

#Updating Fact table (simple update) Transactions
#curs.execute ("""insert into DEMIPT2.DRNC_DWH_FACT_TRANSACTIONS (trans_id, trans_date, card_num, oper_type, amt, oper_result, terminal_id )
#select trans_id, trans_date, card_num, oper_type, amt, oper_result, terminal_id from demipt2.drnc_stg_transactions
#""")

#Updating Fact table (simple update) Passports
curs.execute ("""insert into DEMIPT2.DRNC_DWH_FACT_PSSPRT_BLCKLST (PASSPORT_NUM, entry_dt)
select PASSPORT_NUM, entry_dt from demipt2.drnc_stg_PSSPRT_BLCKLST
""")

# Updating META TABLES
curs.execute ("""update demipt2.drnc_meta_bank
set last_update_dt = ( select max(update_dt) from demipt2.DRNC_STG_CARDS )
where table_db = 'DEMIPT2' and table_name = 'CARDS' and ( select max(update_dt) from demipt2.DRNC_STG_CARDS) is not null
""")
curs.execute ("""update demipt2.drnc_meta_bank
set last_update_dt = ( select max(update_dt) from demipt2.drnc_stg_accounts )
where table_db = 'DEMIPT2' and table_name = 'ACCOUNTS' and ( select max(update_dt) from demipt2.drnc_stg_accounts) is not null
""")
curs.execute ("""update demipt2.drnc_meta_bank
set last_update_dt = ( select max(update_dt) from demipt2.drnc_stg_clients )
where table_db = 'DEMIPT2' and table_name = 'CLIENTS' and ( select max(update_dt) from demipt2.drnc_stg_clients) is not null
""")
curs.execute("""update demipt2.drnc_meta_bank
set last_update_dt = ( select max(update_dt) from demipt2.drnc_stg_terminals )
where table_db = 'DEMIPT2' and table_name = 'TERMINALS' and ( select max(update_dt) from demipt2.drnc_stg_terminals) is not null
""")

curs.execute("""update demipt2.drnc_meta_bank
set last_update_dt = ( select max(trans_date) from demipt2.DRNC_DWH_FACT_TRANSACTIONS )
where table_db = 'DEMIPT2' and table_name = 'TRANSACTIONS' and ( select max(trans_date) from DEMIPT2.DRNC_DWH_FACT_TRANSACTIONS) is not null
""")

curs.execute("""update demipt2.drnc_meta_bank
set last_update_dt = ( select max(entry_dt) from demipt2.drnc_stg_PSSPRT_BLCKLST)
where table_db = 'DEMIPT2' and table_name = 'PSSPRT_BLCKLST' and ( select max(entry_dt) from demipt2.drnc_stg_PSSPRT_BLCKLST) is not null
""")

print ('the ETL process was successfully completed')

# Launching the FRAUD REPORT in SQL 

print ('rendering the report ... just relax...searching for the evil doers')

curs.execute("""
    insert into DEMIPT2.DRNC_REP_FRAUD (EVENT_DT,PASSPORT,FIO,PHONE,EVENT_TYPE,REPORT_DT)
select
    event_dt,
    passport_num,
    fio,
    phone,
    event_type,
    current_date
        from
        (
                ---results for request for fraud number 4 --------------------------------------------------------------------------
                select         
                    date3 as event_dt,
                    passport_num,
                    last_name||' '||upper(substr(first_name,1,1))||'.'||substr(patronymic,1,1)||'.' fio,
                    phone,
                    4 as event_type
                        from(
                                    select 
                                              card_num,
                                              oper_type,
                                              trans_date date3,
                                              lag(trans_date,2) over (partition by card_num order by trans_date) date1,
                                              lag(oper_result, 2) over (partition by card_num order by trans_date) result1, 
                                              lag(oper_result, 1) over (partition by card_num order by trans_date) result2, 
                                              oper_result result3,
                                              lag(amt, 2) over (partition by card_num order by trans_date) amt1,
                                              lag(amt, 1) over (partition by card_num order by trans_date) amt2,
                                              amt amt3
                                    from DEMIPT2.DRNC_DWH_FACT_TRANSACTIONS
                                    where oper_type = 'WITHDRAW'
                                    ) t
                        inner join DRNC_DWH_DIM_CARDS_HIST cr
                                on cr.card_num = t.card_num and cr.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and cr.deleted_flg='N'
                        inner join DRNC_DWH_DIM_ACCOUNTS_HIST p
                                on p.account = cr.account and p.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and p.deleted_flg='N'
                        inner join DRNC_DWH_DIM_CLIENTS_HIST c
                                on c.client_id=p.client and c.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and c.deleted_flg='N'
                        where  result3='SUCCESS' and result2='REJECT' and result1='REJECT' 
                                and amt3<amt2 and amt2<amt1
                                and date3 - interval '20' minute <date1          
              
                union all
                --- plus results for request for fraud number 3----------------------------------------------------------------------
                select 
                            event_dt,
                            passport_num,
                            last_name||' '||upper(substr(first_name,1,1))||'.'||substr(patronymic,1,1)||'.' fio,
                            phone,
                            3 as event_type
                 
                        from (
                                 select
                                     card_num,
                                     min (trans_date) event_dt
                                 from
                                    (   select 
                                              card_num,
                                              trans_date,
                                              terminal_city,
                                              lag(trans_date,1) over (partition by card_num order by trans_date) p_date, 
                                              lag (terminal_city,1) over (partition by card_num order by trans_date) p_city         
                                        from DEMIPT2.DRNC_DWH_FACT_TRANSACTIONS t 
                                        inner join DRNC_DWH_DIM_TERMINALS_HIST tm
                                            on t.terminal_id=tm.terminal_id 
                                            and tm.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and tm.deleted_flg='N'
                                    ) 
                                        where trans_date - interval '30' minute <p_date and terminal_city <>p_city
                                        group by card_num
                                ) f
                        inner join DRNC_DWH_DIM_CARDS_HIST cr
                            on cr.card_num = f.card_num and cr.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and cr.deleted_flg='N'
                        inner join DRNC_DWH_DIM_ACCOUNTS_HIST p
                            on p.account = cr.account and p.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and p.deleted_flg='N'
                        inner join DRNC_DWH_DIM_CLIENTS_HIST c
                            on c.client_id=p.client and c.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and c.deleted_flg='N'         
             
                union all
                --- plus results for request for fraud number 2-------------------------------------------------------------------
                select 
                            event_dt,
                            passport_num,
                            last_name||' '||upper(substr(first_name,1,1))||'.'||substr(patronymic,1,1)||'.' fio,
                            phone,
                            2 as event_type
                 
                from (
                            select 

                                cr.account, 
                                p.client,
                                min( trunc(trans_date,'DD')) as event_dt

                            from DEMIPT2.DRNC_DWH_FACT_TRANSACTIONS t
                            inner join DRNC_DWH_DIM_CARDS_HIST cr
                                on cr.card_num = t.card_num and cr.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and cr.deleted_flg='N'
                            inner join DRNC_DWH_DIM_ACCOUNTS_HIST p
                                on p.account = cr.account and p.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and p.deleted_flg='N'
                            where p.valid_to <trunc(trans_date,'DD')
                            group by cr.account, p.client
                    ) f
                inner join DRNC_DWH_DIM_CLIENTS_HIST c
                                on client=client_id and c.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and c.deleted_flg='N'
            
                union all 
                --- plus results for request for fraud number 1-------------------------------------------------------------------
                select  
                            min( trans_date) as event_dt,
                            passport_num, 
                            max(last_name) ||' '||upper(substr(max(first_name),1,1))||'.'||substr(max(patronymic),1,1)||'.' fio,
                            max(phone),
                            1 as event_type
                from DEMIPT2.DRNC_DWH_FACT_TRANSACTIONS t
                inner join DRNC_DWH_DIM_CARDS_HIST cr
                    on cr.card_num = t.card_num and cr.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and cr.deleted_flg='N'
                inner join DRNC_DWH_DIM_ACCOUNTS_HIST p
                    on p.account = cr.account and p.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and p.deleted_flg='N'
                inner join DRNC_DWH_DIM_CLIENTS_HIST c
                    on client=client_id and c.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and c.deleted_flg='N'
                where passport_num
                    IN
                        (select passport_num from DRNC_DWH_FACT_PSSPRT_BLCKLST) 
                         or coalesce (passport_valid_to, date '9999-12-31')< trunc(trans_date,'DD')
                group by passport_num
         )
where passport_num  not in (select passport from DEMIPT2.DRNC_REP_FRAUD) ---checking if passport numbers are already included in report
    """)

print('Report is ready, sir/maam')

# Finalizing

conn.commit()
conn.close()

# Copying files to arhive

print ('creating backup..')

subprocess.call ("find /home/demipt2/drnc -name '*.xlsx'  |xargs -I{} mv {} /home/demipt2/drnc/archive",shell=True)

subprocess.call ("find /home/demipt2/drnc/archive -name '*.xlsx'  |xargs -I{} mv {} {}.backup",shell=True)

subprocess.call ("find /home/demipt2/drnc -name '*.txt'  |xargs -I{} mv {} /home/demipt2/drnc/archive",shell=True)

subprocess.call ("find /home/demipt2/drnc/archive -name '*.txt'  |xargs -I{} mv {} {}.backup",shell=True)


print ('everything is fine now, pack your things and go home')



