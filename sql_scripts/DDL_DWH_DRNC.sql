
--DDL DWH Измерения
--Cards
CREATE TABLE DEMIPT2.DRNC_DWH_DIM_CARDS_HIST (
            CARD_NUM CHAR (20),
            ACCOUNT CHAR (20),
            EFFECTIVE_FROM DATE,
            EFFECTIVE_TO DATE,
            DELETED_FLG CHAR(1)
); 
--Accounts
CREATE TABLE DEMIPT2.DRNC_DWH_DIM_ACCOUNTS_HIST(
             ACCOUNT CHAR (20),
             VALID_TO DATE,
             CLIENT VARCHAR2 (20),
             EFFECTIVE_FROM DATE,
             EFFECTIVE_TO DATE,
             DELETED_FLG CHAR(1)
);
--Клиенты
CREATE TABLE  DEMIPT2.DRNC_DWH_DIM_CLIENTS_HIST(
            CLIENT_ID VARCHAR2 (20),
            LAST_NAME VARCHAR2 (100),
            FIRST_NAME VARCHAR2 (100),
            PATRONYMIC VARCHAR2 (100),
            DATE_OF_BIRTH DATE,
            PASSPORT_NUM VARCHAR2 (15),
            PASSPORT_VALID_TO DATE,
            PHONE   VARCHAR2 (20),
            EFFECTIVE_FROM DATE,
            EFFECTIVE_TO DATE,
            DELETED_FLG CHAR(1)
);

--Терминалы
CREATE TABLE  DEMIPT2.DRNC_DWH_DIM_TERMINALS_HIST(
            terminal_id VARCHAR2 (20),
            terminal_type VARCHAR2 (100),
            terminal_city VARCHAR2 (100),
            terminal_address VARCHAR2 (100),
            effective_from DATE,
            effective_to DATE,
            deleted_flg CHAR(1)
);


--DDL DWH Факты
---TRANSACTIONS
CREATE TABLE DEMIPT2.DRNC_DWH_FACT_TRANSACTIONS(
            trans_id VARCHAR2 (20),
            trans_date DATE,
            card_num CHAR (20),
            oper_type VARCHAR2 (100),
            amt decimal,
            oper_result VARCHAR2 (15),
            terminal_id VARCHAR2 (20)
);

--- DEMIPT2.DRNC_PSSPRT_BLCKLST

CREATE TABLE DEMIPT2.DRNC_DWH_FACT_PSSPRT_BLCKLST(
            PASSPORT_NUM VARCHAR2 (15),
            entry_dt DATE
            
);
---
CREATE TABLE DEMIPT2.DRNC_REP_FRAUD (
            EVENT_DT DATE,
            PASSPORT VARCHAR2 (15),
            FIO VARCHAR2 (250),
            PHONE VARCHAR2 (20),
            EVENT_TYPE CHAR (1),
            REPORT_DT DATE
); 

--- Создаем таблицу Метаданных и вносим стартовые записи

create table demipt2.drnc_meta_bank(
    table_db varchar2(30),
    table_name varchar2(30),
    last_update_dt date
    
);

insert into demipt2.drnc_meta_bank(table_db, table_name, last_update_dt) values ( 'DEMIPT2', 'CARDS',  to_date( '1899-12-31', 'YYYY-MM-DD') );
insert into demipt2.drnc_meta_bank(table_db, table_name, last_update_dt) values ( 'DEMIPT2', 'ACCOUNTS',  to_date( '1899-12-31', 'YYYY-MM-DD') );
insert into demipt2.drnc_meta_bank(table_db, table_name, last_update_dt) values ( 'DEMIPT2', 'CLIENTS',  to_date( '1899-12-31', 'YYYY-MM-DD') );
insert into demipt2.drnc_meta_bank(table_db, table_name, last_update_dt) values ( 'DEMIPT2', 'TERMINALS',  to_date( '1899-12-31', 'YYYY-MM-DD') );
insert into demipt2.drnc_meta_bank(table_db, table_name, last_update_dt) values ( 'DEMIPT2', 'TRANSACTIONS',  to_date( '1899-12-31', 'YYYY-MM-DD') );
insert into demipt2.drnc_meta_bank(table_db, table_name, last_update_dt) values ( 'DEMIPT2', 'PSSPRT_BLCKLST',  to_date( '1899-12-31', 'YYYY-MM-DD') );