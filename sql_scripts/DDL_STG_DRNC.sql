-- DDL таблиц стейджинга  
--- DEMIPT2.DRNC_STG_CARDS
Create table demipt2.drnc_stg_cards (
            CARD_NUM CHAR (20),
            ACCOUNT CHAR (20),
            UPDATE_DT DATE
); 
--- DEMIPT2.DRNC_STG_ACCOUNTS
Create table demipt2.drnc_stg_accounts(
             ACCOUNT CHAR (20),
             VALID_TO DATE,
             CLIENT VARCHAR2 (20),
             UPDATE_DT DATE
);
--- DEMIPT2.DRNC_STG_CLIENTS
Create table demipt2.drnc_stg_clients(
            CLIENT_ID VARCHAR2 (20),
            LAST_NAME VARCHAR2 (100),
            FIRST_NAME VARCHAR2 (100),
            PATRONYMIC VARCHAR2 (100),
            DATE_OF_BIRTH DATE,
            PASSPORT_NUM VARCHAR2 (15),
            PASSPORT_VALID_TO DATE,
            PHONE   VARCHAR2 (20),
            UPDATE_DT DATE
);


--- DEMIPT2.DRNC_STG_TERMINALS
Create table demipt2.drnc_stg_terminals(
            terminal_id VARCHAR2 (20),
            terminal_type VARCHAR2 (100),
            terminal_city VARCHAR2 (100),
            terminal_address VARCHAR2 (100),
            update_dt DATE
);

--- DEMIPT2.DRNC_STG_TRANSACTIONS
Create table demipt2.drnc_stg_transactions(
            trans_id VARCHAR2 (20),
            trans_date DATE,
            card_num CHAR (20),
            oper_type VARCHAR2 (100),
            amt decimal,
            oper_result VARCHAR2 (15),
            terminal_id VARCHAR2 (20)
);

--- DEMIPT2.DRNC_STG_PSSPRT_BLCKLST

Create table demipt2.drnc_stg_PSSPRT_BLCKLST(
            PASSPORT_NUM VARCHAR2 (15),
            entry_dt DATE
            
);

-- Таблицы удалений

create table demipt2.drnc_stg_cards_del(
    card_num char (20)
);

create table demipt2.drnc_stg_accounts_del(
     account char (20)
);

create table demipt2.drnc_stg_clients_del(
      client_id varchar2 (20)
);

create table demipt2.drnc_stg_terminals_del(
      terminal_id varchar2 (20)
);

---Таблица Source
Create table demipt2.drnc_source_terminals(
            terminal_id VARCHAR2 (20),
            terminal_type VARCHAR2 (100),
            terminal_city VARCHAR2 (100),
            terminal_address VARCHAR2 (100),
            update_dt DATE
);